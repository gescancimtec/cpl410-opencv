# CPL410-OpenCV
This repository contains a vagrant virtual machine to help you create encodings files for facial recognition.  The vagrant machine contains everything you need to make this happen. 

Once you have provisioned the VM with `vagrant up` the provisioner will create a directory in the same folder as your Vagrantfile called `facialrecognition`.  Inside this directory is the dataset.

To generate a new dataset, create a directory for each person you in the dataset directory.  Ensure you have at least 5-6 front facing images of the persons face.  Then ssh into the vagrant box and run the following:

```Shell
cd ~/facialrecognition
python3 encode_faces.py --dataset dataset --encodings encodings.pickle --detection-method hog
```

This will create a new encodings.pickle file which you can access from your base machine in the facialrecognition directory.  Copy this to the facialrecognition system you are using.

If you want to test the dataset before copying it, you can use the recognize.py script.  First copy an image into the facialrecognition folder.  This image should contain a person who exists in your dataset.  We reccomend not using a dataset photo, but instead a separate test photo.  Once the image has been copied into the facialrecognition folder, from inside the VM run:

```Shell
cd ~/facialrecognition
python3 recognize.py --image filename.jpeg --casecade haarvascade_frontalface_default.xml --encodings encodings.pickle
```
This should create an image called result.png showing the result of the facial recognition.